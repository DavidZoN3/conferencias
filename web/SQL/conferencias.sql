-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2016 a las 22:27:05
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21
 
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `conferencias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conferencias`
--

CREATE TABLE `conferencias` (
  `IDCONFERENCIAS` int(11) NOT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `FECHACONFERENCIA` date DEFAULT NULL,
  `HORAFIN` time DEFAULT NULL,
  `HORAINICIO` time DEFAULT NULL,
  `NOMBRECONFERENCIA` varchar(255) DEFAULT NULL,
  `NOMBREPONENTE` varchar(255) DEFAULT NULL,
  `TIPOCONFERENCIA` varchar(255) DEFAULT NULL,
  `salas_idSalas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `IDGRUPO` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`IDGRUPO`) VALUES
('root'),
('usuario'),
('encSalas'),
('encConferencias'),
('encRegistros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salas`
--

CREATE TABLE `salas` (
  `IDSALAS` int(11) NOT NULL,
  `CAPACIDAD` int(11) DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `NOMBRESALAS` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `NOMBREUSUARIO` varchar(255) NOT NULL,
  `APELLIDOMATERNOUSUARIO` varchar(255) DEFAULT NULL,
  `APELLIDOPATERNOUSUARIO` varchar(255) DEFAULT NULL,
  `CONTRASENNA` varchar(255) DEFAULT NULL,
  `CORREOUSUARIO` varchar(255) DEFAULT NULL,
  `NOMBRESUSUARIO` varchar(255) DEFAULT NULL,
  `grupos_idGrupos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`NOMBREUSUARIO`, `APELLIDOMATERNOUSUARIO`, `APELLIDOPATERNOUSUARIO`, `CONTRASENNA`, `CORREOUSUARIO`, `NOMBRESUSUARIO`, `GRUPOS_IDGRUPOS`) VALUES
('erickweeds', 'Rosas', 'Sandoval', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'erickweeds@gmail.com', 'Erick Sandoval','encRegistros'),
('rodrigo', 'xxxx', 'xxx', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'xx', 'xx','encSalas'),
('usuario', 'usuario', 'usuario', '9250e222c4c71f0c58d4c54b50a880a312e9f9fed55d5c3aa0b0e860ded99165', 'usuario', 'usuario','usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_conferencias`
--

CREATE TABLE `usuarios_has_conferencias` (
  `usuarios_nombreUsuario` varchar(45) NOT NULL,
  `conferencias_idconferencias` int(11) NOT NULL,
  `asistencias` tinyint(1) DEFAULT '0',
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conferencias`
--
ALTER TABLE `conferencias`
  ADD PRIMARY KEY (`IDCONFERENCIAS`),
  ADD KEY `FK_CONFERENCIAS_salas_idSalas` (`salas_idSalas`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`NOMBREUSUARIO`),
  ADD KEY `FK_USUARIOS_grupos_idGrupos` (`grupos_idGrupos`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`IDGRUPO`);

--
-- Indices de la tabla `salas`
--
ALTER TABLE `salas`
  ADD PRIMARY KEY (`IDSALAS`);

--
-- Indices de la tabla `usuarios_has_conferencias`
--
ALTER TABLE `usuarios_has_conferencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuarios_has_conferencias_conferencias1_idx` (`conferencias_idconferencias`),
  ADD KEY `fk_usuarios_has_conferencias_usuarios1_idx` (`usuarios_nombreUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `conferencias`
--
ALTER TABLE `conferencias`
  MODIFY `IDCONFERENCIAS` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `salas`
--
ALTER TABLE `salas`
  MODIFY `IDSALAS` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios_has_conferencias`
--
ALTER TABLE `usuarios_has_conferencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `conferencias`
--
ALTER TABLE `conferencias`
  ADD CONSTRAINT `FK_CONFERENCIAS_salas_idSalas` FOREIGN KEY (`salas_idSalas`) REFERENCES `salas` (`IDSALAS`);

--
-- Filtros para la tabla `usuarios`
--

--
-- Filtros para la tabla `usuarios_has_conferencias`
--
ALTER TABLE `usuarios_has_conferencias`
  ADD CONSTRAINT `fk_usuarios_has_conferencias_conferencias1` FOREIGN KEY (`conferencias_idconferencias`) REFERENCES `conferencias` (`IDCONFERENCIAS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_conferencias_usuarios1` FOREIGN KEY (`usuarios_nombreUsuario`) REFERENCES `usuarios` (`NOMBREUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `confercenter`.`usuarios` 
ADD CONSTRAINT `fk_usuarios_grupos`
  FOREIGN KEY (`grupos_idGrupos`)
  REFERENCES `confercenter`.`grupos` (`IDGRUPO`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;