/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davidsc
 */
@Entity
@Table(name = "usuarios_has_conferencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosHasConferencias.findAll", query = "SELECT u FROM UsuariosHasConferencias u"),
    @NamedQuery(name = "UsuariosHasConferencias.findByAsistencias", query = "SELECT u FROM UsuariosHasConferencias u WHERE u.asistencias = :asistencias"),
    @NamedQuery(name = "UsuariosHasConferencias.findById", query = "SELECT u FROM UsuariosHasConferencias u WHERE u.id = :id")})
public class UsuariosHasConferencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "asistencias")
    private Boolean asistencias;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "conferencias_idconferencias", referencedColumnName = "IDCONFERENCIAS")
    @ManyToOne(optional = false)
    private Conferencias conferenciasIdconferencias;
    @JoinColumn(name = "usuarios_nombreUsuario", referencedColumnName = "NOMBREUSUARIO")
    @ManyToOne(optional = false)
    private Usuarios usuariosnombreUsuario;

    public UsuariosHasConferencias() {
    }

    public UsuariosHasConferencias(Integer id) {
        this.id = id;
    }

    public Boolean getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(Boolean asistencias) {
        this.asistencias = asistencias;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Conferencias getConferenciasIdconferencias() {
        return conferenciasIdconferencias;
    }

    public void setConferenciasIdconferencias(Conferencias conferenciasIdconferencias) {
        this.conferenciasIdconferencias = conferenciasIdconferencias;
    }

    public Usuarios getUsuariosnombreUsuario() {
        return usuariosnombreUsuario;
    }

    public void setUsuariosnombreUsuario(Usuarios usuariosnombreUsuario) {
        this.usuariosnombreUsuario = usuariosnombreUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosHasConferencias)) {
            return false;
        }
        UsuariosHasConferencias other = (UsuariosHasConferencias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.confercenter.model.UsuariosHasConferencias[ id=" + id + " ]";
    }
    
}
