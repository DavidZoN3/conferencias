/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Erick
 */
@Entity
@Table(name = "conferencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conferencias.findAll", query = "SELECT c FROM Conferencias c"),
    @NamedQuery(name = "Conferencias.findByIdconferencias", query = "SELECT c FROM Conferencias c WHERE c.idconferencias = :idconferencias"),
    @NamedQuery(name = "Conferencias.findByDescripcion", query = "SELECT c FROM Conferencias c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Conferencias.findByFechaconferencia", query = "SELECT c FROM Conferencias c WHERE c.fechaconferencia = :fechaconferencia"),
    @NamedQuery(name = "Conferencias.findByHorafin", query = "SELECT c FROM Conferencias c WHERE c.horafin = :horafin"),
    @NamedQuery(name = "Conferencias.findByHorainicio", query = "SELECT c FROM Conferencias c WHERE c.horainicio = :horainicio"),
    @NamedQuery(name = "Conferencias.findByNombreconferencia", query = "SELECT c FROM Conferencias c WHERE c.nombreconferencia = :nombreconferencia"),
    @NamedQuery(name = "Conferencias.findByNombreponente", query = "SELECT c FROM Conferencias c WHERE c.nombreponente = :nombreponente"),
    @NamedQuery(name = "Conferencias.findByTipoconferencia", query = "SELECT c FROM Conferencias c WHERE c.tipoconferencia = :tipoconferencia")})
public class Conferencias implements Serializable {

    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCONFERENCIAS")
    private Integer idconferencias;
    @Size(max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "FECHACONFERENCIA")
    @Temporal(TemporalType.DATE)
    private Date fechaconferencia;
    @Column(name = "HORAFIN")
    @Temporal(TemporalType.TIME)
    private Date horafin;
    @Column(name = "HORAINICIO")
    @Temporal(TemporalType.TIME)
    private Date horainicio ;
    @Size(max = 255)
    @Column(name = "NOMBRECONFERENCIA")
    private String nombreconferencia;
    @Size(max = 255)
    @Column(name = "NOMBREPONENTE")
    private String nombreponente;
    @Size(max = 255)
    @Column(name = "TIPOCONFERENCIA")
    private String tipoconferencia;
    @JoinColumn(name = "salas_idSalas", referencedColumnName = "IDSALAS")
    @ManyToOne
    private Salas salasidSalas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conferenciasIdconferencias")
    private List<UsuariosHasConferencias> usuariosHasConferenciasList;

    public Conferencias() {
    }

//    public Boolean validarConferencia (String fechaConferencia, String horaInicio, String horaFinal, int idSala){
//       boolean ocupado = false;
//        if (em.createNamedQuery("Conferencias.findByFechaHoraInicioHoraFianl")
//                .setParameter("fechaconferencia", fechaConferencia)
//                .setParameter("horainicio", horaInicio)
//                .setParameter("horafinal", horaFinal)
//                .setParameter("salasidSalas", idSala)
//                .getResultList() != null) {
//            ocupado = true;
//        } else {
//        }
//        
//        return ocupado;
//                
//    }
    public Conferencias(Integer idconferencias) {
        this.idconferencias = idconferencias;
    }

    public Integer getIdconferencias() {
        return idconferencias;
    }

    public void setIdconferencias(Integer idconferencias) {
        this.idconferencias = idconferencias;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaconferencia() {
        return fechaconferencia;
    }

    public void setFechaconferencia(Date fechaconferencia) {
        this.fechaconferencia = fechaconferencia;
    }

    public Date getHorafin() {
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(horainicio);
        calendario.add(Calendar.HOUR, 1);
        setHorafin(calendario.getTime());
        setHorafin(calendario.getTime());
        return calendario.getTime();
    }

    public void setHorafin(Date horafin) {
        this.horafin = horafin;
    }

    public Date getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(Date horainicio) {
        this.horainicio = horainicio;
    }

    public String getNombreconferencia() {
        return nombreconferencia;
    }

    public void setNombreconferencia(String nombreconferencia) {
        this.nombreconferencia = nombreconferencia;
    }

    public String getNombreponente() {
        return nombreponente;
    }

    public void setNombreponente(String nombreponente) {
        this.nombreponente = nombreponente;
    }

    public String getTipoconferencia() {
        return tipoconferencia;
    }

    public void setTipoconferencia(String tipoconferencia) {
        this.tipoconferencia = tipoconferencia;
    }

    public Salas getSalasidSalas() {
        return salasidSalas;
    }

    public void setSalasidSalas(Salas salasidSalas) {
        this.salasidSalas = salasidSalas;
    }

    @XmlTransient
    public List<UsuariosHasConferencias> getUsuariosHasConferenciasList() {
        return usuariosHasConferenciasList;
    }

    public void setUsuariosHasConferenciasList(List<UsuariosHasConferencias> usuariosHasConferenciasList) {
        this.usuariosHasConferenciasList = usuariosHasConferenciasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconferencias != null ? idconferencias.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conferencias)) {
            return false;
        }
        Conferencias other = (Conferencias) object;
        if ((this.idconferencias == null && other.idconferencias != null) || (this.idconferencias != null && !this.idconferencias.equals(other.idconferencias))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.confercenter.model.Conferencias[ idconferencias=" + idconferencias + " ]";
    }

}
