/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author davidsc
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarios.findAll", query = "SELECT u FROM Usuarios u"),
    @NamedQuery(name = "Usuarios.findByNombreusuario", query = "SELECT u FROM Usuarios u WHERE u.nombreusuario = :nombreusuario"),
    @NamedQuery(name = "Usuarios.findByApellidomaternousuario", query = "SELECT u FROM Usuarios u WHERE u.apellidomaternousuario = :apellidomaternousuario"),
    @NamedQuery(name = "Usuarios.findByApellidopaternousuario", query = "SELECT u FROM Usuarios u WHERE u.apellidopaternousuario = :apellidopaternousuario"),
    @NamedQuery(name = "Usuarios.findByContrasenna", query = "SELECT u FROM Usuarios u WHERE u.contrasenna = :contrasenna"),
    @NamedQuery(name = "Usuarios.findByCorreousuario", query = "SELECT u FROM Usuarios u WHERE u.correousuario = :correousuario"),
    @NamedQuery(name = "Usuarios.findByNombresusuario", query = "SELECT u FROM Usuarios u WHERE u.nombresusuario = :nombresusuario")})
public class Usuarios implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuariosnombreUsuario")
    private Collection<UsuariosHasConferencias> usuariosHasConferenciasCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NOMBREUSUARIO")
    private String nombreusuario;
    @Size(max = 255)
    @Column(name = "APELLIDOMATERNOUSUARIO")
    private String apellidomaternousuario;
    @Size(max = 255)
    @Column(name = "APELLIDOPATERNOUSUARIO")
    private String apellidopaternousuario;
    @Size(max = 255)
    @Column(name = "CONTRASENNA")
    private String contrasenna;
    @Size(max = 255)
    @Column(name = "CORREOUSUARIO")
    private String correousuario;
    @Size(max = 255)
    @Column(name = "NOMBRESUSUARIO")
    private String nombresusuario;
    @JoinColumn(name = "grupos_idGrupos", referencedColumnName = "IDGRUPO")
    @ManyToOne(optional = false)
    private Grupos gruposidGrupos;

    //private Grupos gruposidGrupos;
    
//    private String gruposidGrupo;
//
//    public String getGruposidGrupo() {
//        return gruposidGrupo;
//    }
//
//    public void setGruposidGrupo(String gruposidGrupo) {
//        this.gruposidGrupo = gruposidGrupo;
//    }

    public Usuarios() {
    }

    public Usuarios(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }
    
    public Usuarios(String nombreusuario,String apellidomaternousuario,String apellidopaternousuario, String contrasenna,String correousuario,String nombresusuario /*, String gruposidGrupo*/){
        this.nombreusuario=nombreusuario;
        this.apellidomaternousuario=apellidomaternousuario;
        this.apellidopaternousuario=apellidopaternousuario;
        this.contrasenna=contrasenna;
        this.correousuario=correousuario;
        this.nombresusuario=nombresusuario;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }
    

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public String getApellidomaternousuario() {
        return apellidomaternousuario;
    }

    public void setApellidomaternousuario(String apellidomaternousuario) {
        this.apellidomaternousuario = apellidomaternousuario;
    }

    public String getApellidopaternousuario() {
        return apellidopaternousuario;
    }

    public void setApellidopaternousuario(String apellidopaternousuario) {
        this.apellidopaternousuario = apellidopaternousuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public String getCorreousuario() {
        return correousuario;
    }

    public void setCorreousuario(String correousuario) {
        this.correousuario = correousuario;
    }

    public String getNombresusuario() {
        return nombresusuario;
    }

    public void setNombresusuario(String nombresusuario) {
        this.nombresusuario = nombresusuario;
    }

    public Grupos getGruposidGrupos() {
        return gruposidGrupos;
    }

    public void setGruposidGrupos(Grupos gruposidGrupos) {
        this.gruposidGrupos = gruposidGrupos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombreusuario != null ? nombreusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.nombreusuario == null && other.nombreusuario != null) || (this.nombreusuario != null && !this.nombreusuario.equals(other.nombreusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.confercenter.model.Usuarios[ nombreusuario=" + nombreusuario + " ]";
    }

    @XmlTransient
    public Collection<UsuariosHasConferencias> getUsuariosHasConferenciasCollection() {
        return usuariosHasConferenciasCollection;
    }

    public void setUsuariosHasConferenciasCollection(Collection<UsuariosHasConferencias> usuariosHasConferenciasCollection) {
        this.usuariosHasConferenciasCollection = usuariosHasConferenciasCollection;
    }
    
}