/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author davidsc
 */
@Entity
@Table(name = "salas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salas.findAll", query = "SELECT s FROM Salas s"),
    @NamedQuery(name = "Salas.findByIdsalas", query = "SELECT s FROM Salas s WHERE s.idsalas = :idsalas"),
    @NamedQuery(name = "Salas.findByCapacidad", query = "SELECT s FROM Salas s WHERE s.capacidad = :capacidad"),
    @NamedQuery(name = "Salas.findByDescripcion", query = "SELECT s FROM Salas s WHERE s.descripcion = :descripcion"),
    @NamedQuery(name = "Salas.findByNombresalas", query = "SELECT s FROM Salas s WHERE s.nombresalas = :nombresalas")})
public class Salas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDSALAS")
    private Integer idsalas;
    @Column(name = "CAPACIDAD")
    private Integer capacidad;
    @Size(max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 255)
    @Column(name = "NOMBRESALAS")
    private String nombresalas;
    @OneToMany(mappedBy = "salasidSalas")
    private Collection<Conferencias> conferenciasCollection;

    public Salas() {
    }

    public Salas(Integer idsalas) {
        this.idsalas = idsalas;
    }

    public Integer getIdsalas() {
        return idsalas;
    }

    public void setIdsalas(Integer idsalas) {
        this.idsalas = idsalas;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombresalas() {
        return nombresalas;
    }

    public void setNombresalas(String nombresalas) {
        this.nombresalas = nombresalas;
    }

    @XmlTransient
    public Collection<Conferencias> getConferenciasCollection() {
        return conferenciasCollection;
    }

    public void setConferenciasCollection(Collection<Conferencias> conferenciasCollection) {
        this.conferenciasCollection = conferenciasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsalas != null ? idsalas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salas)) {
            return false;
        }
        Salas other = (Salas) object;
        if ((this.idsalas == null && other.idsalas != null) || (this.idsalas != null && !this.idsalas.equals(other.idsalas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return   nombresalas;
    }
    
}
