/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.controllers;

//import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.confercenter.beans.SalasFacade;
import org.confercenter.model.Salas;

/**
 *
 * @author erickrodrigo
 */
@Named(value = "registroSalas")
@RequestScoped
public class RegistroSalas {

    @Inject
    private SalasFacade registrar;
    private Salas select = new Salas();

    public String guardar() {
        try {
            Salas obj = new Salas();
            obj.setIdsalas(id);
            obj.setNombresalas(NombreSala);
            obj.setCapacidad(Capacidad);
            obj.setDescripcion(Descripcion);
            this.registrar.create(obj);
            this.id = obj.getIdsalas();

        } catch (Exception exs) {
            FacesContext faces = FacesContext.getCurrentInstance();
            faces.addMessage(null, new FacesMessage("Nombre de sala existente"));
            return "RegistroSalas";
        }
        FacesContext faces = FacesContext.getCurrentInstance();
        faces.addMessage(null, new FacesMessage("Sala agregada correctamente"));
        return "ActualizarSalas";
    }

    public List<Salas> getSalas() {
        return this.registrar.findAll();
    }

    /**
     * Creates a new instance of RegistroSalas
     */
    public RegistroSalas() {
    }

    private int id;
    private String NombreSala;
    private int Capacidad = 50;
    private String Descripcion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreSala() {
        return NombreSala;
    }

    public void setNombreSala(String NombreSala) {
        this.NombreSala = NombreSala;
    }

    public int getCapacidad() {
        return Capacidad;
    }

    public void setCapacidad(int Capacidad) {
        this.Capacidad = Capacidad;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
}
