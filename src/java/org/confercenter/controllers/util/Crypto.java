/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.controllers.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick
 */
public class Crypto {

    public static String encryptString(String target) {
        String llave = "";
        try {

            MessageDigest digest;
            byte[] hash;
            int l = 0;
            StringBuilder hexString = new StringBuilder();

            digest = MessageDigest.getInstance("SHA-256");
            hash = digest.digest(target.getBytes());
            l = hash.length;
            for (int i = 0; i < l; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            llave = hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Crypto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return llave;
    }
    public static void main(String[] args) {
        System.out.println(Crypto.encryptString("12345"));
    }
}
