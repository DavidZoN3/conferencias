/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.confercenter.beans.ConferenciasFacade;
import org.confercenter.model.Conferencias;

/**
 *
 * @author erickrodrigo
 */
@Named(value = "registroConferencias")
@RequestScoped
public class RegistroConferencias {

    @Inject
    private ConferenciasFacade registro;
    private Conferencias select = new Conferencias();

    private int idConf;
    private String nombreConf;
    private String nombrePonente;
    private String descripcion;
    private String tipoConf;
    private Date fechaCon;
    private Date horai = new Date();
    private Date horaf;
    private int idSala;
    private Date estrablecerHoraFinal;
    Calendar calendario = Calendar.getInstance();
    ////

    public String guardar() {

        Conferencias obj = new Conferencias();
        System.out.println(getIdConf());
        obj.setIdconferencias(idConf);
        System.out.println(getNombreConf());
        obj.setNombreconferencia(nombreConf);
        System.out.println(getNombrePonente());
        obj.setNombreponente(nombrePonente);
        System.out.println(getDescripcion());
        obj.setDescripcion(descripcion);
        System.out.println(getTipoConf());
        obj.setTipoconferencia(tipoConf);
        System.out.println(getFechaCon());
        obj.setFechaconferencia(fechaCon);
        System.out.println(getHorai());
        obj.setHorainicio(horai);
        System.out.println(getHoraf());
        obj.setHorafin(horaf);
        System.out.println(getIdSala());
        obj.setSalasidSalas(select.getSalasidSalas());

        this.registro.create(obj);
        this.idConf = obj.getIdconferencias();

        return "ActualizarConferencias";

    }

//    public Date getEstrablecerHoraFinal() {
//       
//    }
    public void setEstrablecerHoraFinal(Date estrablecerHoraFinal) {
        this.estrablecerHoraFinal = estrablecerHoraFinal;
    }

    public List<Conferencias> getConferencias() {
        return this.registro.findAll();
    }

    public int getIdConf() {
        return idConf;
    }

    public void setIdConf(int idConf) {
        this.idConf = idConf;
    }

    public String getNombreConf() {
        return nombreConf;
    }

    public void setNombreConf(String nombreConf) {
        this.nombreConf = nombreConf;
    }

    public String getNombrePonente() {
        return nombrePonente;
    }

    public void setNombrePonente(String nombrePonente) {
        this.nombrePonente = nombrePonente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoConf() {
        return tipoConf;
    }

    public void setTipoConf(String tipoConf) {
        this.tipoConf = tipoConf;
    }

    public Date getFechaCon() {
        return fechaCon;
    }

    public void setFechaCon(Date fechaCon) {
        this.fechaCon = fechaCon;
    }

    public Date getHorai() {

        return horai;
    }

    public void setHorai(Date horai) {
        this.horai = horai;
    }

    public Date getHoraf() {
        calendario.setTime(horai);
        calendario.add(Calendar.HOUR, 1);
        setHoraf(calendario.getTime());
        setHoraf(calendario.getTime());
        return calendario.getTime();
    }

    public void setHoraf(Date horaf) {
        this.horaf = horaf;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    /**
     * Creates a new instance of RegistroConferencias
     */
    public RegistroConferencias() {
    }

}
