/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.beans;

import java.io.Serializable;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.confercenter.controllers.UsuariosController;
import org.confercenter.model.Grupos;
import org.confercenter.model.Usuarios;

/**
 *
 * @author davidsc
 */
@Named(value = "usersBean")
@RequestScoped
public class UsersBean implements Serializable{

//    @Inject
//    private UsuariosFacade registrar;
//    private Usuarios select = new Usuarios(); 
    
    private String users;
    private String nombre;
    private String apaterno;
    private String amaterno;
    private String contrasenna;
    private String email;
    private String grupoid;
    private boolean activo;
    
    private Usuarios usuarioLog;
    private Collection<Usuarios> listaUsuario;

    @Inject
    UsuariosController usuario;
    
    Usuarios user,userMod;
    Grupos grupUser;
    
    @PostConstruct
    private void init() {
        this.activo = true;
    }
    

//    public UsuariosFacade getRegistrar() {
//        return registrar;
//    }

//    public void setRegistrar(UsuariosFacade registrar) {
//        this.registrar = registrar;
//    }

//    public Usuarios getSelect() {
//        return select;
//    }
//
//    public void setSelect(Usuarios select) {
//        this.select = select;
//    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrupoid() {
        return grupoid;
    }

    public void setGrupoid(String grupoid) {
        this.grupoid = grupoid;
    }
    
    
    UsuariosController E = new UsuariosController();
    public void Guardar(){
        System.out.println("*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*sdddddddddddddddddddddddddddd-*-*-*-*-*-*-*-*-*");
        contrasenna = E.encripta(contrasenna);
        user = new Usuarios(users,amaterno,apaterno,contrasenna,email,nombre);
        Grupos g = new Grupos("usuario");
        user.setGruposidGrupos(g);
        System.out.println("-->"+users+"--"+amaterno+"--"+apaterno+"--"+contrasenna+"--"+email+"--"+nombre);
        
       // user = usuario.getUsuarios(user.getNombreusuario());
        if(user!=null){
            System.out.println("-->"+users+"--"+amaterno+"--"+apaterno+"--"+contrasenna+"--"+email+"--"+nombre);
            usuario.setSelected(user);
            System.out.println("-->"+users+"--"+amaterno+"--"+apaterno+"--"+contrasenna+"--"+email+"--"+nombre);
            usuario.create(); 
        }else{
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ya existe un usuario registrado con ese Username","Error"));
            
        }
    }
    
    public UsersBean() {
        
    }    
}
