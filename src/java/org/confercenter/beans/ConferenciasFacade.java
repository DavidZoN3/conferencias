/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.confercenter.model.Conferencias;

/**
 *
 * @author Erick
 */
@Stateless
public class ConferenciasFacade extends AbstractFacade<Conferencias> {
    @PersistenceContext(unitName = "ConferCenterPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConferenciasFacade() {
        super(Conferencias.class);
    }
    
}
