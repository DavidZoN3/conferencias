/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.confercenter.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.confercenter.model.UsuariosHasConferencias;

/**
 *
 * @author Erick
 */
@Stateless
public class UsuariosHasConferenciasFacade extends AbstractFacade<UsuariosHasConferencias> {
    @PersistenceContext(unitName = "ConferCenterPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosHasConferenciasFacade() {
        super(UsuariosHasConferencias.class);
    }
    
}
