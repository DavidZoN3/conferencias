/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Calendar;
 
import java.util.Date;
import java.util.GregorianCalendar;
import javax.faces.bean.ManagedBean;



@ManagedBean
public class ValidarCalendario {

    Calendar calendario = new GregorianCalendar();
    String dia;
    String mes;
    String anio;
    String fechaActual = null;

    public Calendar getCalendario() {
        return calendario;
    }

    public void setCalendario(Calendar calendario) {
        this.calendario = calendario;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getFechaActual() {
        dia = Integer.toString(calendario.get(Calendar.DATE));
        mes = Integer.toString(calendario.get(Calendar.MONTH) + 1);
        anio = Integer.toString(calendario.get(Calendar.YEAR));
        fechaActual = mes + "/" + dia + "/" + anio;
        System.out.println("Fecha Actual:" + fechaActual);
        return fechaActual;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }
}
